### <a href="http://bit.ly/agave-togo">Agave ToGo</a>  

Get a head start on your next development sprint by leveraging the open source <a href="http://bit.ly/agave-togo" title="Agave ToGo" target="_blank">Agave ToGo</a> project. This bare bones webapp can be reused in your existing project or used as-is for a clean, responsive, client-side web application that brings the full power of Agave to your browser.

### Web API  

Manage apps, jobs, and data. Share anything, generate reports, track usage, and manage your digital lab through <a href="http://agaveapi.co/documentation/live-docs/" title="Live Documentation">Agave's RESTful web APIs</a>.

### Client SDK  

The <a href="http://agaveapi.co/tools/client-sdk/" title="Client SDK">Agave Client SDK</a> makes it easy to add auth, job, data, and collaboration features to your Java, Python, PHP, R, and Perl apps.

### Command Line Interface (CLI)  

Tap into the full power of the Agave Web API and integrate Agave into your existing scripts and workflows from the command line using the <a href="http://agaveapi.co/tools/command-line-interface/" title="Agave Command Line Interface (CLI)">Agave CLI</a>.