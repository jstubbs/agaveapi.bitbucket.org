<aside class="notice">Note that by using Agave developer tools, you accept our <a title="Terms of Service" href="/terms-of-service/">Developer Terms of Service</a>.</aside>

[half]
<p style="text-align: center;"><img class="aligncenter" src="/wp-content/themes/agave/images/getting-started.png" alt="" width="102" height="102" /></p>

<h2 style="text-align: center;">Getting started  
<div>
<ul>
	<li><strong><a title="REST API User Guide" href="http://agaveapi.co/documentation/user-guide/">User Guide</a></strong>: A quick overview of the Web API to get you started.</li>
</ul>
<ul>
	<li><strong><a title="Beginner’s Guides" href="http://agaveapi.co/documentation/beginners-guides/">Beginner Guides</a></strong>: Step-by-step guides to creating your first Web API application.</li>
</ul>
<ul>
	<li><strong><a title="Client Registration" href="http://agaveapi.co/documentation/tutorials/client-registration/">My Applications</a></strong>: Register your app and get keys for authenticating your requests.</li>
</ul>
<ul>
	<li><strong><a href="/code-examples/">Code examples</a></strong>: Get demos, wrappers, and tools for your programming language.</li>
</ul>
</div>
[/half][half-last]
<p style="text-align: center;"><img class="aligncenter" src="/wp-content/themes/agave/images/reference2.png" alt="" width="102" height="102" /></p>

<h2 id="reference" style="text-align: center;">Reference  
<div>
<ul>
	<li><strong><a title="Live Documentation" href="http://agaveapi.co/documentation/live-docs/">Live Docs</a></strong>: Full, interactive documentation of all the endpoints and the data they return.</li>
</ul>
<ul>
	<li><strong><a title="Authorization Guide" href="http://agaveapi.co/documentation/authorization/">Authorization Guide</a></strong>: All you need to know about using OAuth 2.0 with the Web API.</li>
</ul>
<ul>
	<li><strong><a title="Advanced Tutorials" href="http://agaveapi.co/documentation/tutorials/">Advanced Tutorials</a></strong>: Deep-dive tutorials on all aspects of the REST APIs.</li>
</ul>
<ul>
	<li><strong><a href="/migration-guide/">Migration Guide</a></strong>: Tips on migrating from v1 to v2.</li>
</ul>
</div>
[/half-last]