Agave always has been and always will driven by community needs and specific requests for features and enhancements. If you have a request for a new API, feature, or way the platform could be improved, please let us know by submitting a ticket on our issue tracker on <a href="https://bitbucket.org/taccaci/agave/issues">Bitbucket</a>.

For your convenience, all current requests are shown below. This list is pulled from our issue tracker and displayed in near real time, so what you see here is really what's on our list.

[bitbucket-issues kind="enhancement" status="new,open"]