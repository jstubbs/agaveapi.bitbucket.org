---
title: Agave Platform Developer Documentation

language_tabs:
  - shell: cURL
  - plaintext: CLI

toc_footers:
  - <a href='https://public.agaveapi.co/signup'>Sign Up for a Developer Key</a>
  - <a href='https://bitbucket.org/taccaci/agave/src/2.1.5/CHANGELOG.md' target='_blank'>CHANGELOG</a>
  - <a href='https://bitbucket.org/taccaci/agave/issues?reported_by=%21closed&reported_by=proposal&reported_by=enhancement&status=new&status=open' target='_blank'>Road Map</a>

includes:
  - introduction
  - documentation/_authorization-guide
  - documentation/_event-reference
  - documentation/_search-guide
  - documentation/_user-guide
  - documentation/beginners-guides/_app-discovery
  - documentation/beginners-guides/_index

search: true
---
